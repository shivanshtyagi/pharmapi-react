import React from 'react';
import axios from "axios";


class PharmaAjout extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        day: 'lundi',
        name: 'Pharmacie ...',
        city: '',
        quartier: '',
    }
  
      
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
      this.setState({
          [event.target.name]: event.target.value
      });
  }

    handleSubmit(event){
      event.preventDefault();
      axios.post('http://127.0.0.1:8000/pharma', { 
          nom : this.state.name,
          garde : this.state.day,
          ville : this.state.city,
          quartier : this.state.quartier
      });
      alert('Ajout de la Pharmacie : ' + this.state.name)
    }

  
  
    render() {
      return (
        <form onSubmit={this.handleSubmit} style={{ margin: '0 auto', width: '550px',  padding: '1em', border: '1px solid #CCC', borderRadius:'1em',background: 'rgba( 255, 9, 179, 0.25 )',boxShadow: '0 8px 32px 0 rgba( 31, 38, 135, 0.37 )',backdropFilter: 'blur( 20.0px )',webkitBackdropFilter: 'blur( 20.0px )',borderRadius: '10px',border: '1px solid rgba( 255, 255, 255, 0.18 )'}}>
          <div class="mb-3">
            <label>
              Nom : <input type="text" name="name" value={this.state.name} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}}/>
            </label>
          </div>
          <div class="mb-3">
            <label>
              Ville : <input type="text" name="city" value={this.state.city} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}}/>
            </label>  
          </div>   
          <div class="mb-3">   
            <label>
              Quartier : <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}} />
            </label>
          </div>
          <div class="mb-3">
            <label style={{width: '300px', boxSizing: 'border-box'}}>
              Jour de garde : 
              <select value={this.state.day} name="day" onChange={this.handleChange}>
                <option value="Lundi">Lundi</option>
                <option value="Mardi">Mardi</option>
                <option value="Mercredi">Mercredi</option>
                <option value="Jeudi">Jeudi</option>
                <option value="Vendredi">Vendredi</option>
                <option value="Samedi">Samedi</option>
                <option value="Dimanche">Dimanche</option>
    
              </select>
            </label>
          </div>
          <input type="submit" value="Ajout"/>
        </form>
      );
    }
  }
  

  export default PharmaAjout;