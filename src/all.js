import React, { Component } from 'react';
import PharmaList from './PharmaList.js';

class all extends Component {
  render() {
    return (
        <div>
          <h2 style={{ color: '#567ace', textDecoration: 'underline' }}>Toute les pharmacies :</h2>
          <PharmaList />
        </div>
    );
  }
}

export default all;