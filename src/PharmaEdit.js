import React from 'react';
import axios from "axios";
import PharmaList from './PharmaList.js';


class PharmaEdit extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        day: '',
        name: '',
        city: '',
        quartier: '',
        edit: false
    }
  
      
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    componentDidMount() {
        axios.get(`http://127.0.0.1:8000/pharma`)
          .then(res => { 
                res.data.map(pharmacies => {
                    {pharmacies.id === this.props.id && 
                        this.setState({
                            name : pharmacies.nom,
                            quartier : pharmacies.quartier,
                            city : pharmacies.ville,
                            day : pharmacies.garde
                        })
                    };
                 });
            });
        }
                            
      

    handleChange(event){
      this.setState({
          [event.target.name]: event.target.value
      });
  }

    handleSubmit(event){
      event.preventDefault();
      axios.put('http://127.0.0.1:8000/pharma/' + this.props.id, { 
          nom : this.state.name,
          garde : this.state.day,
          ville : this.state.city,
          quartier : this.state.quartier
      })
      .catch(function(error){
          console.log(error)
      })
      .then(()=>{
        alert('Ajout de la Pharmacie : ' + this.state.name)
      })
    }

  
  
    render() {
      return (
        <div>
            {this.state.edit === false &&
            <>
                <form onSubmit={this.handleSubmit} style={{ margin: '0 auto', width: '550px',  padding: '1em', border: '1px solid #CCC', borderRadius:'1em',boxShadow: '0 8px 32px 0 rgba( 31, 38, 135, 0.37 )',backdropFilter: 'blur( 20.0px )',webkitBackdropFilter: 'blur( 20.0px )',borderRadius: '10px',border: '1px solid rgba( 255, 255, 255, 0.18 )'}}>
                    <div class="mb-3">
                        <label>
                        Nom : <input type="text" name="name" value={this.state.name} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}}/>
                        </label>
                    </div>
                    <div class="mb-3">
                        <label>
                        Ville : <input type="text" name="city" value={this.state.city} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}}/>
                        </label>  
                    </div>   
                    <div class="mb-3">   
                        <label>
                        Quartier : <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange} style={{width: '300px', boxSizing: 'border-box'}}/>
                        </label>
                    </div>
                    <div class="mb-3" >
                        <label>
                        Jour de garde : 
                        <select value={this.state.day} name="day" onChange={this.handleChange} >
                            <option value="lundi">Lundi</option>
                            <option value="mardi">Mardi</option>
                            <option value="mercredi">Mercredi</option>
                            <option value="jeudi">Jeudi</option>
                            <option value="vendredi">Vendredi</option>
                            <option value="samedi">Samedi</option>
                            <option value="dimanche">Dimanche</option>
                
                        </select>
                        </label>
                    </div>
                    <input type="submit" value="Ajout"/>
                </form>
            </>
            }
            {this.state.edit === true && <PharmaList/>}
        </div>
      );
    }
  }
  

  export default PharmaEdit;