import './App.css';
import React, {useState} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import all from './all';
import garde from './garde';
import add from './add';


function App() {
  const [show, setShow]=useState(true)

  return (
    <div className="App">
      <header>        
        <div style={{height: '150px',fontStyle: 'bold', fontSize:'40px',position: 'static'}}>
          <p>Pharmapi React</p>
        </div>
      </header>
      <nav className="App-header">
        <Router>
          <div>
            <nav>
              <ul>
                <li><Link to={'/'} className="nav-link"> Home </Link></li>
                <Link to={'/all'} className="nav-link"><button onClick={()=>setShow(!show)}>Toute les pharmacies</button></Link>
                <Link to={'/garde'} className="nav-link"><button onClick={()=>setShow(!show)}>La pharmacie de garde</button></Link>
                <Link to={'/add'} className="nav-link"><button onClick={()=>setShow(!show)}>Ajouter pharmacie</button></Link>
              </ul>
            </nav>
            <hr />
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/all' component={all} />
                <Route path='/garde' component={garde} />
                <Route path='/add' component={add} />
            </Switch>
          </div>
        </Router>

      </nav>
      <footer> 
        <div style={{height: '150px'}}>
            <p> Crée par Shivansh </p>
        </div>
      </footer>
      
    </div>
  );

}

export default App;

