import React from 'react';
import axios from "axios";




class PharmaGarde extends React.Component {

    constructor(props){
        super(props);
        this.state = {pharmacies: []};
    
      
      }
    


  componentDidMount() {
    axios.get(`http://127.0.0.1:8000/pharma-garde`)
      .then(res => {
         const pharmapi = res.data;
        console.log(pharmapi.data);
        console.log(pharmapi);
        this.setState({ pharmacies: pharmapi});
      })
  }

  render() {
    return (
        <div>
        { this.state.pharmacies.map(pharmacies => 
          <div style={{ border: '1px solid #CCC', borderRadius:'10px' }}>
              <h2>{pharmacies.nom}</h2>
              <p>{pharmacies.quartier}</p>
              <p>{pharmacies.ville}</p>
              <p>{pharmacies.garde}</p>
          </div>
        )}
    </div>
    )
  }
}
export default PharmaGarde;
